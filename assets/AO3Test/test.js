QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing calculate function with several sets of inputs', function (assert) {
  assert.equal(calculator(12, 2, 1), 23.88, 'Tested with three positive numbers');
    assert.equal(calculator(20, 0, 0), 0, 'Tested with three 0.');
  assert.equal(calculator(-20, 10, 2), "Error", 'Tested with two negative number and a number.');
 assert.equal(calculator("q", "a", -5), "Error", 'Tested with two string and a number.');
});

// QUnit.test('Testing taxAmount function with several sets of inputs', function (assert) {
//     assert.equal(calculator(12, 2, 1), 23.88, 'Tested with two positive numbers');
//     assert.equal(function () {calculator(100, 100, 10), 9990; }, /Income and tax percentage cannot be 0 or less/, 'Passing in a negative value correctly raises an Error');    
//     // assert.throws(function () {calculator(-1, -10); }, /Income and tax percentage cannot be 0 or less/, 'Passing in two negative values correctly raises an Error');
//     // assert.throws(function () {calculator("Test", 5); }, /Both income and tax percentage have to be numbers/, 'Passing in a string correctly raises an Error');    
// });
